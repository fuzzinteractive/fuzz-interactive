<?php get_header(); ?>

<?php include (TEMPLATEPATH . "/sidebar_left.php"); ?>

	<div id="content" class="narrowcolumn" role="main">

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

		<?php
		if ( has_post_thumbnail() ) {
			// the current post has a thumbnail
		} else {
			// the current post lacks a thumbnail
		}
		?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

				<div class="thumb">
				<a href="<?php the_permalink() ?>"><?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(180,120), array("class" => "alignleft post_thumbnail")); } ?></a>
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small><?php the_time('F jS, Y') ?> <!-- by <?php the_author() ?> --></small>

				</div>

				<div class="entry">
				<?php the_content(''); ?>				
				</div>

			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>

<?php get_footer(); ?>
