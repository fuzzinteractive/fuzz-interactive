<?php
/*
Template Name: Patterns
*/
?>

<?php get_header(); ?>

<?php include (TEMPLATEPATH . "/sidebar_left.php"); ?>

	<div id="content" class="narrowcolumn" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post2" id="post-<?php the_ID(); ?>">
		<h2><?php the_title(); ?></h2>
			<div class="entry">
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			</div>

		<div id="columnwrap">
		<div id="column">
		<?php if ( !function_exists('dynamic_sidebar')
		|| !dynamic_sidebar('column1') ) : ?>
		<?php endif; ?>
		</div>

		<div id="column">
		<?php if ( !function_exists('dynamic_sidebar')
		|| !dynamic_sidebar('column2') ) : ?>
		<?php endif; ?>
		</div>

		<div id="column">
		<?php if ( !function_exists('dynamic_sidebar')
		|| !dynamic_sidebar('column3') ) : ?>
		<?php endif; ?>
		</div>
		</div>

	</div>

		<?php endwhile; endif; ?>
	
	</div>

<?php include (TEMPLATEPATH . "/sidebar.php"); ?>

<?php get_footer(); ?>
