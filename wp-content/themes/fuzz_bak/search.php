<?php

get_header(); ?>

	<div id="content" class="narrowcolumn" role="main">

	<?php if (have_posts()) : ?>

		<h2 class="pagetitle">Search Results</h2>

		<?php
		if ( has_post_thumbnail() ) {
			// the current post has a thumbnail
		} else {
			// the current post lacks a thumbnail
		}
		?>

		<?php while (have_posts()) : the_post(); ?>
			
			<div id="results">

				<div class="thumb">
				<a href="<?php the_permalink() ?>"><?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) { the_post_thumbnail(array(180,120), array("class" => "alignleft post_thumbnail")); } ?></a>
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small><?php the_time('F jS, Y') ?> <!-- by <?php the_author() ?> --></small>
				</div>

				<div class="entry">
				<?php the_content('More Info &raquo;'); ?>				
				</div>
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">You're Out of Luck. Try a different search?</h2>

	<?php endif; ?>

	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
