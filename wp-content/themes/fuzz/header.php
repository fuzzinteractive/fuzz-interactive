<?php
/**
 * The Header for our theme.
 *
 * @package _s
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/style.css">
<script type="text/javascript" src="//use.typekit.net/rzt8jwp.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<?php wp_head(); ?>
<script>
jQuery(document).ready(function(){
	var myDevice = jQuery(window).width()
	 if(myDevice < 800 ) {
    	jQuery('.royalSlider .rsImg').each(function() {
	        var element = $(this); // your IMG, or A tag (for lazy-loaded images)
	        var src = $(this).attr('src');
	        element.attr('src', src.replace('1920x640','800x600')); // this replaces the larger (1200) file by the smaller (800) file
   		});
   	}
 
 });
 </script>
</head>

<body <?php body_class(); ?>>

	<?php
		$homelink = get_bloginfo('url'); ?>
	?>



	<header class="full-primary header">
		<nav class="burger-nav">
			<ul>
				<li><a href="<?php echo get_home_url(); ?>#work">Portfolio</a></li>
                <li><a href="<?php echo get_home_url(); ?>#blog">Blog</a></li>
				<li><a href="<?php echo get_home_url(); ?>#about">Work With Us</a></li>
				<li><a href="<?php echo get_home_url(); ?>#contact">Contact</a></li>
			</ul>
		</nav>
		<div class="row">
			<nav class="primary-burger hide-for-medium-up">
				<svg id ="svg-burger" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="32" viewBox="0 0 32 32">
	<path d="M2 6h28v6h-28zM2 14h28v6h-28zM2 22h28v6h-28z" fill="#ffffff" />
</svg>
			</nav>
			<div class="logo-contain columns small-4 large-3">
				<a href = "/">
					<img class="logo" src="<?php echo get_template_directory_uri(); ?>/library/images/logo-fuzz-interactive.png" alt="Fuzz Interactive" />
				</a>
			</div>
			<nav class="primary-nav columns large-9 show-for-medium-up">
				<ul>
					<li><a href="<?php echo get_home_url(); ?>#work">Portfolio</a></li>
                    <li><a href="<?php echo get_home_url(); ?>#blog">Blog</a></li>
					<li><a href="<?php echo get_home_url(); ?>#about">Work With Us</a></li>
					<li><a href="<?php echo get_home_url(); ?>#contact">Contact</a></li>
				</ul>
			</nav> 
		</div>
	</header>