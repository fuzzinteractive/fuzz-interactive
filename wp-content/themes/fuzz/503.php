<!DOCTYPE html>

<head>
<title>fuzzinteractive.com | coming soon</title>
<link rel="stylesheet" href="http://fuzzinteractive.com/wp-content/themes/fuzz_bak/splash.css" type="text/css" media="screen" />

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5511796-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>
<div id="border">
	<div id="north"></div>
	<div id="east"></div>
	<div id="west"></div>
	<div id="south"></div>
</div>

<div id="logo"><img src="http://fuzzinteractive.com/wp-content/themes/fuzz_bak/images/logo.png"></div>
<a href="https://plus.google.com/106995463459545020217" rel="publisher"></a>
</body>

</html>