<?php
/**
 * Template Name: Front Page
 *
 *
 * @package _s
 */

get_header(); ?>

    <section class="intro-statement">
    	<div class="statement">
    		<div class="fuzz-loader">
    			<img alt="loading slideshow" src="<?php echo get_template_directory_uri(); ?>/library/images/ajax-loader.gif">
    		</div>
	       <div class="cycle-slideshow">
				<?php 
				$args = array( 'post_type' => 'home_page_featured', 'posts_per_page' => 5 );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
						echo '<li>';
							echo '<div class="entry-content">';
							the_content();
							echo '</div>';
						echo '</li>';
				endwhile;
				wp_reset_postdata();
				?>
			</div>
		</div>
    </section>

    <!--START PORTFOLIO -->
    <div id="yellow-block"></div>
    <section class="latest-work" id="work"> 
	       <?php query_posts('post_type=home_portfolio'); ?>
	       <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	 	     <?php the_content(); ?>
	       <?php endwhile; endif; ?>
	       <?php wp_reset_query(); ?>
    </section>

	<!--START FEED -->
    <div id="dgrey-block"></div>
	<section class="blog-contain" id="blog">
        <div class="title"><h5>Follow Us</h5></div>
        <div class="row full-width">
        	<nav class = "slide-control" id="insta-prev"></nav>
			<nav class = "slide-control" id="insta-next"></nav>
        	<div class="post-slideshow">
			<?php 
				query_posts('posts_per_page=20');
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post(); ?>
						
						<?php get_template_part( 'content', 'blog' ); ?>

	                <?php   	
					} // end while
				} // end if
			?>
			</div>
        </div>
	</section>
                
	<!--START QUOTES -->


	<!--<section class="full-secondary quotes">
		<div class="row"> -->
	
			
			<!--add CYCLE 2 -->

			<!--<div class="quote-item">
				<h3>Good Design is the Silent Seller</h3>
				<span class="quote-author">Shane Meendering</span>
			</div> -->


		<!--</div>
	</section> -->


	<section class="team" id="about">
        
        <div id="dred-block"></div>
        <div class="workwithus">
        <h1>We specialize in:</h1>
        <h6>Brand Development, Art Direction, 2D Motion,<br> 
            3D Modeling & Animation, Web Design & Development.</h6>
        </div>
        <div id="yellow-block"></div>
        <div class="brands">
            <h3>Some of the brands we've worked with:</h3>
            <img src="<?php echo get_template_directory_uri(); ?>/library/images/clients.jpg">
        </div>
		<div class="row full-width">
            
			<div class="large-4 columns team-member">
                            <span class="roller"> </span>
	                	 	<div class="team-title-contain">
                                <p>Founder, Creative Director</p>
		                    </div>
				<a href="#"><img src="http://placekitten.com/534/534" /></a>
			</div>

			<div class="large-4 columns team-member">
                            <span class="roller"> </span>
	                	 	<div class="team-title-contain">
                                <p>Director Of Operations</p>
		                    </div>
				<a href="#"><img src="http://placekitten.com/534/534" /></a>
			</div>

			<div class="large-4 columns team-member">
                            <span class="roller"> </span>
	                	 	<div class="team-title-contain">
                                <p>Development Lead</p>
		                    </div>
				<a href="#"><img src="http://placekitten.com/534/534" /></a>
			</div>
            
		</div>
        	               
	</section>

    <section class="contact" id="contact">
    <div class="row">
      <div class="fuzz-black"></div>
      <div class="large-6 columns large-centered">
        <?php echo do_shortcode( '[contact-form-7 id="8" title="Fuzz Interactive Contact Form"]' ); ?>
      </div>
    </div>
    <?php get_footer(); ?>
    </section>

