<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _s
 */

get_header(); ?>

<section class="blog-contain-full">
<div class="row">
    <div class="large-8 columns">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
    </div>
    <div class="large-3 columns">
    <?php get_sidebar(); ?>
    </div>
</div>
</section>
<?php get_footer(); ?>