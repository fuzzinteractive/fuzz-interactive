<?php
/**
 * The template used for displaying blog content
 * @package _s
 */
?>

				<?php
					$categories = get_the_category();
					$separator = ' ';
					$output = '';

					if($categories){
						foreach($categories as $category) {
							$output .= '<span class="blog-cat">'.$category->cat_name.'</span>';
						} 
					} ?>
					<div class="blog-item">
						<a href="<?php the_permalink(); ?>">
							<?php
							if ( get_the_post_thumbnail($post_id) != '' ) {

							   the_post_thumbnail('blog-thumb');

							} else {

							 echo '<img src="';
							 echo catch_that_image();
							 echo '" alt="" />';

		                    } ?>

		               
	            
	                	 	<span class="roller"> </span>
	                	 	<div class="blog-title-contain">
		                     	<h2><?php the_title(); ?></h2>
		                     	<span class="blog-cat"><?php echo $output; ?></span>
		                    </div>
		                </a>
	                </div>