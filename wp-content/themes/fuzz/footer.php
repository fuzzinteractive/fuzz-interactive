<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _s
 */
?>

<div class="footertype"><p>@2013 Fuzz LLC   |   PH: 971.277.3989 <br>
ALL RIGHTS RESERVED</p></div>

<?php wp_footer(); ?>

<script>

jQuery(document).ready(function(){
	jQuery('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
	        || location.hostname == this.hostname) {

	        var target = jQuery(this.hash);
	        target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
	           if (target.length) {
	             jQuery('html,body').animate({
	                 scrollTop: target.offset().top
	            }, 1000);
	            return false;
	        }
	    }
	});
});
</script>

</body>
</html>